注意：如果 react 版本在 v17 以下，那么使用 react-devtools-v3 会报错，原因是 react-devtools 版本过低，需要使用 react-devtools-v4 版本

# 一、方式一：通过 gitee 获取最终扩展程序（v3、v4）

### 1. 通过 gitee 下载扩展程序

1. 下载地址

```
https://gitee.com/wwjwuweijie/react-redux-devtools?_from=gitee_search
```

直接下载 zip 安装包，里面包括 React-Developer Tools-3.6.0、React-Developer Tools-4.9.0、Redux-DevTools-2.17.1 三个扩展程序（此目录为最终编译后目录，下载后直接通过 chrome 扩展程序加载既可）

### 2. 在 chrome 浏览器上加载扩展程序

1. 查看当前项目的 react 版本，选择对应扩展程序
   如果是 v17 以下版本，使用 React-Developer Tools-3.6.0 扩展程序
   如果是 v17 以上版本，使用 React-Developer Tools-4.9.0 扩展程序
2. 打开 chrome 浏览器上加载扩展程序
   打开 Google Chrome 浏览器 --> 右上角三个点图标 --> 更多工具 --> 扩展程序 --> 加载已解压的扩展程序 --> 选择扩展程序
3. npm start 运行项目

# 二、方式二：到 github 下载 react-devtools 源码，安装依赖、解压、加载扩展程序（v3）

### 1. 到 github 上下载 react-devtools-v3

[下载地址](https://github.com/facebook/react-devtools/tree/v3)，下载成功后解压，文件夹名为 react-devtools-3

### 2. 将 文件夹名为 react-devtools-3 编译成 chrome 浏览器扩展程序可用文件

1. 在 react-devtools-3 目录下启动终端，安装依赖

```
npm install
```

2. 依赖安装成功后打包编译

```
npm run build:extension:chrome
```

3. 将打包后的 shells\chrome\build\unpacked 目录加载到 chrome 扩展程序
   打开 Google Chrome 浏览器 --> 右上角三个点图标 --> 更多工具 --> 扩展程序 --> 加载已解压的扩展程序 --> 选择 shells\chrome\build\unpacked 目录
4. npm start 运行项目
